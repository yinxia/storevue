import Vue from 'vue'
import Router from 'vue-router'
Vue.use(Router)

const mian = () => import('./views/mian.vue')
const WXsmallroutine = () => import('./views/WXsmallroutine.vue')
const store = () => import('./views/indexstore.vue')
const order = () => import('./views/order.vue')
const preferential = () => import('./views/preferential.vue')
// const Recruits = () => import('./views/Recruits.vue')

export default new Router({
  routes: [
   {
    path: '/',
    name: 'home',
    component: mian,
    redirect:{name:'store'},
    children:[
      {

        path: 'store',
        name: 'store',
        component: store,
        meta: {
          keepAlive: true
        }
      },
      {
        path: 'WXsmallroutine',
        name: 'WXsmallroutine',
        component: WXsmallroutine,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'order',
        name: 'order',
        component: order,
        meta: {
            keepAlive: true
        }      
      },
      {
        path: 'preferential',
        name: 'preferential',
        component: preferential,
        meta: {
            keepAlive: true
        }      
      }
    ]
  }
 ]
})