import axios from './serve'  // 普通请求


export const saveWXcartoon = (data) => {
    return axios.post('/api/storeRoundImg/saveOrUpdate', data)
};

export const cartoonList = (data) => {
    return axios.get('/api/storeRoundImg/getPagerAllList', { params: data })
};

export const cartoonDel = (data) => {
    return axios.delete('/api/storeRoundImg/del', { params: data })
};

export const details = (data) => {
    return axios.get('/api/storeRoundImg/details', { params: data })
};

/**
 *   轮播管理--改变状态
 * @export
 * @param {*} data ——数据
 */
export const cartoonChangeState  = (data) => {
    return axios.post('/api/storeRoundImg/changeState', data)
};

