import axios from 'axios';
// import {Loading, Message, MessageBox} from 'element-ui';
// import qs from 'qs';
import {getCookie} from "../unit";


let token = getCookie('access_token');
const $upload = axios.create()
$upload.defaults.timeout = 5000;
$upload.defaults.withCredentials = true;
$upload.defaults.baseURL = process.env.VUE_APP_URL;
// http://zhinao.bjike.com:8031/
// http://192.168.0.185:8031/
// process.env.VUE_APP_URL

$upload.interceptors.request.use((config) => { 
        config.headers = {
            'Authorization': `${token}`,
            // 'Content-Type': 'application/x-www-form-urlencoded'
            "Content-Type": "multipart/form-data"
        }
        // config.data = qs.stringify(config.data, {arrayFormat: 'repeat'});
    return config;
});
// 响应拦截器
// $http.interceptors.response.use(data => {
//     return data
// }, error => {
//     if (error && error.code) {
//         switch (error.code) {
//             case 2:
//                 Loading.service(options).close();
//                 Message({
//                     message: '未授权，登录状态已经过期,请重新登录',
//                     type: 'error',
//                     duration: 3000,
//                     onClose() {
//                         return window.location.href = "http://zhinao.bjike.com/login?url=" + window.location.href;
//                     }
//                 });
//         }
//     }
//     if (error && error.request.status) {
//         switch (error.request.status) {
//             case 400:
//                 error.message = '错误请求';
//                 break;
//             case 401:
//                 Loading.service(options).close();
//                 Message({
//                     message: '未授权，请重新登录',
//                     type: 'error',
//                     duration: 3000,
//                     onClose() {
//                         return window.location.href = "http://zhinao.bjike.com/login?url=" + window.location.href;
//                     }
//                 });
//                 return;
//             case 403:
//                 error.message = '拒绝访问';
//                 break;
//             case 404:
//                 error.message = '请求错误,未找到该资源';
//                 break;
//             case 405:
//                 error.message = '请求方法未允许';
//                 break;
//             case 408:
//                 error.message = '请求超时';
//                 break;
//             case 500:
//                 error.message = '服务器端出错';
//                 break;
//             case 501:
//                 error.message = '网络未实现';
//                 break;
//             case 502:
//                 error.message = '网络错误';
//                 break;
//             case 503:
//                 error.message = '服务不可用';
//                 break;
//             case 504:
//                 error.message = '网络超时';
//                 break;
//             case 505:
//                 error.message = '抱歉！您当前的浏览器版本过低，可能存在安全风险，请升级浏览器至IE10及以上版本。';
//                 break;
//             default:
//                 error.message = `连接错误${error.status}`
//         }
//         Loading.service(options).close();
//         Message({
//             message: error.message,
//             type: 'error',
//             duration: 3000
//         });
//         return Promise.reject(error.response.data)
//     }

//     return Promise.reject(error)
// });
export default $upload
