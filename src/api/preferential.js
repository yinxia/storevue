import axios from './serve'  // 普通请求


export const saveOrupdatePreferential = (data) => {
    return axios.post('/api/preferential/saveOrUpdate', data)
};

export const preferentialList = (data) => {
    return axios.get('/api/preferential/getPagerAllList', data)
};

export const preferentialDel = (data) => {
    return axios.delete('/api/preferential/del', { params: data })
};

export const details = (data) => {
    return axios.get('/api/preferential/details', { params: data })
};



