#!/bin/bash
git pull

# 删除原容器
docker rm -f baidu-vue

# 删除原镜像
docker rmi -f baidu-vue

yarn run build

# 构建镜像
docker build -t baidu-vue -f Dockerfile .

# 启动容器
docker run -d -p 8092:8092 --name baidu-vue baidu-vue
