// =========== 改进的 start ==============
const fs = require('fs');
const time = new Date().getTime();
const path = require('path');

const devMode = process.env.NODE_ENV !== 'production';
// const SERVER_DOMAIN_NAME = process.env.VUE_APP_SERVER_DOMAIN_NAME;
// const WEB_DOMAIN_PUBLIC = process.env.VUE_APP_WEB_DOMAIN_PUBLIC;
// const PROJECT_TYPE = process.env.VUE_APP_PROJECT_TYPE;


// =========== 改进的 end ==============

function resolve(dir) {
  return path.join(__dirname, dir);
}

module.exports = {
  baseUrl: './',
  outputDir: 'dist',
  //  webpack配置节点入口1
  /* configureWebpack: {
    externals: {
      // jquery: 'jquery',
      // $: 'jquery',
    },
    resolve: {
      alias: {
        RouteConfig: path.resolve(__dirname, `src/router/${PROJECT_TYPE}`),
        elementTheme: path.resolve(__dirname, `src/assets/theme/${PROJECT_TYPE}`),
        '~': path.resolve(__dirname, 'src'),
      },
    },
    output: {
      filename: `js/[name].js?t=${time}`,
      chunkFilename: `js/[name].js?t=${time}`,
    },
    plugins: [
      new webpack.ProvidePlugin({
        // other modules
        introJs: ['intro.js', 'introJs'],
      }),
    ],
  }, */

  // ==== webpack配置节点入口2
  chainWebpack: config => {
    config.module
      .rule('vue')
      .use('vue-loader')
      .loader('vue-loader')
      .tap(options => {
        return options
      });
    // config.resolve.alias
    //   .set('@@v', resolve('src/views'))
    //   .set('@@c', resolve('src/components'))
    //   .set('@@s', resolve('src/api'))
    //   .set('@@s', resolve('src/sever'))
    //   .set('@@A', resolve('src/assets'));
  },
  //  开发服务配置
  devServer: {
    port: 8093,// 运行端口
    // proxy: {
    //   '/api': {
    //     target: 'http://192.168.0.185:8090/',
    //     secure: false, // 如果是https接口，需要配置这个参数
    //     changeOrigin: true, // 是否跨域
    //     pathRewrite: {
    //       '^/apis': '',
    //     }, // 需要rewrite的,
    //     ws: true,
    //   },
    // },
    //  请求前配置
    before(app) {
      app.all('*', (req, res, next) => {
        res.header('Access-Control-Allow-Origin', '*');
        res.header('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
        res.header('Access-Control-Allow-Credentials', 'true');
        next();
      });
    },
    // 代理服务配置。
    proxy: {
      '/api': {
        // target: 'http://192.168.0.185:8090/', // 接口域名
        target: 'http://yinxia138.com/', // 接口域名
        changeOrigin: true, // 表示是否跨域
        pathRewrite: { '^/api': '' }, // 表示需要rewrite重写的,
      },
    },
    disableHostCheck: true,
  },
  // 取消eslint检测
  lintOnSave: false,
  productionSourceMap: false,

};
